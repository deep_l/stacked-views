package com.cred.stackcards.listener;

public interface FragmentPlaceholderClickListener {
    public void onFragmentPlaceholderClicked(int fragmentId);
}
