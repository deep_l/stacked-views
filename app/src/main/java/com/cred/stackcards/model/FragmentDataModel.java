package com.cred.stackcards.model;

public class FragmentDataModel {

    private String fragmentPlaceholderTitle;
    private String fragmentHint;
    private String fragmentTitle;
    private int fragmentId;


    public FragmentDataModel(String fragmentHint, String fragmentTitle) {
        this.fragmentHint = fragmentHint;
        this.fragmentTitle = fragmentTitle;
    }

    public FragmentDataModel(String fragmentPlaceholderTitle, int fragmentId) {
        this.fragmentPlaceholderTitle = fragmentPlaceholderTitle;
        this.fragmentId = fragmentId;
    }

    public String getFragmentPlaceholderTitle() {
        return fragmentPlaceholderTitle;
    }

    public void setFragmentPlaceholderTitle(String fragmentPlaceholderTitle) {
        this.fragmentPlaceholderTitle = fragmentPlaceholderTitle;
    }

    public String getFragmentHint() {
        return fragmentHint;
    }

    public void setFragmentHint(String fragmentHint) {
        this.fragmentHint = fragmentHint;
    }

    public int getFragmentId() {
        return fragmentId;
    }

    public void setFragmentId(int fragmentId) {
        this.fragmentId = fragmentId;
    }

    public String getFragmentTitle() {
        return fragmentTitle;
    }

    public void setFragmentTitle(String fragmentTitle) {
        this.fragmentTitle = fragmentTitle;
    }
}
