package com.cred.stackcards;

public class Constants {

    public static final String ARG_FRAGMENT_ID = "fragment_id";
    public static final String ARG_PLACEHOLDER_FRAGMENT_TITLE = "placeholder_fragment_title";
    public static final String ARG_FRAGMENT_TITLE = "fragment_title";
    public static final String ARG_FRAGMENT_HINT = "fragment_hint";
}
