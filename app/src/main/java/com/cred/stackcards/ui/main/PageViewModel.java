package com.cred.stackcards.ui.main;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.cred.stackcards.model.FragmentDataModel;

public class PageViewModel extends ViewModel {

    private MutableLiveData<Integer> mIndex = new MutableLiveData<>();
    private MutableLiveData<String> mTitle = new MutableLiveData<>();
    private MutableLiveData<FragmentDataModel> mFragmentDataModel = new MutableLiveData<>();
    private LiveData<String> mText = Transformations.map(mIndex, new Function<Integer, String>() {
        @Override
        public String apply(Integer input) {
            return "Hello world from section: " + input;
        }
    });


    public void setIndex(int index) {
        mIndex.setValue(index);
    }

    public void setTitle(String title) {
        mTitle.setValue(title);
    }

    public void setmFragmentDataModel(FragmentDataModel fragmentDataModel) {
        mFragmentDataModel.setValue(fragmentDataModel);
    }

    public LiveData<String> getText() {
        return mText;
    }

    public LiveData<String> getTitle() {
        return mTitle;
    }

    public LiveData<FragmentDataModel> getFragmentLiveData(){
        return mFragmentDataModel;
    }


}