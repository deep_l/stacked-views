package com.cred.stackcards.ui.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.cred.stackcards.R;
import com.cred.stackcards.model.FragmentDataModel;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.cred.stackcards.Constants.ARG_FRAGMENT_HINT;
import static com.cred.stackcards.Constants.ARG_FRAGMENT_ID;
import static com.cred.stackcards.Constants.ARG_PLACEHOLDER_FRAGMENT_TITLE;
import static com.cred.stackcards.Constants.ARG_FRAGMENT_TITLE;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {


    @BindView(R.id.etInput)
    EditText etSectionInput;
    @BindView(R.id.btnSubmit)
    AppCompatButton btnSectionInputSubmit;
    @BindView(R.id.tvFragmentTitle)
    TextView tvTitle;

    private PageViewModel pageViewModel;
    private String placeHolderTitle = null, hint = null, fragmentTitle = null;
    private int fragmentId = 0;

    public static PlaceholderFragment newInstance() {
        return new PlaceholderFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(PageViewModel.class);
        if (getArguments() != null) {
            fragmentId = getArguments().getInt(ARG_FRAGMENT_ID);
            placeHolderTitle = getArguments().getString(ARG_PLACEHOLDER_FRAGMENT_TITLE, "");
            fragmentTitle = getArguments().getString(ARG_FRAGMENT_TITLE, "");
            hint = getArguments().getString(ARG_FRAGMENT_HINT, "");
        }
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, root);

        if (fragmentTitle != null && !fragmentTitle.isEmpty())
            tvTitle.setText(fragmentTitle);

        //pre-fill if there's an already existing value
        if (placeHolderTitle != null && !placeHolderTitle.isEmpty())
            etSectionInput.setText(placeHolderTitle);
        else
            etSectionInput.setHint(hint);

        btnSectionInputSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etSectionInput.getText().toString().isEmpty()) {
                    pageViewModel.setmFragmentDataModel(new FragmentDataModel(etSectionInput.getText().toString(), fragmentId));
                }
            }
        });
        return root;
    }
}