package com.cred.stackcards.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.cred.stackcards.R;

public class FragmentDataHolderView extends CardView {

    public FragmentDataHolderView(@NonNull Context context) {
        super(context);
    }

    public FragmentDataHolderView(@NonNull Context context, String titleValue) {
        super(context);
        setBackgroundColor(context.getColor(R.color.colorAccent));
        initView(titleValue);
    }

    public FragmentDataHolderView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FragmentDataHolderView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initView(String titleValue){
        LayoutInflater.from(getContext()).inflate(R.layout.fragment_data_placeholder_view,this);
        TextView textView=(TextView)findViewById(R.id.tvPlaceholderView);
        textView.setText(titleValue);

    }

    public void updateView(String titleValue){
        TextView textView=(TextView) ((ViewGroup)this.getChildAt(0)).getChildAt(0);
        textView.setText(titleValue);
    }

}
