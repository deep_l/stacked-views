package com.cred.stackcards.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.cred.stackcards.MainActivity;
import com.cred.stackcards.model.FragmentDataModel;

import java.util.List;

/**
 * Custom View Group to maintain placeholder fields.
 */

public class LinearDataLayout extends LinearLayout {


    public LinearDataLayout(Context context) {
        super(context);
    }

    public LinearDataLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LinearDataLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * Maintaining user input data and adding/updating views to which user can click and modify the data.
     *
     * @param childPosition
     * @param value
     * @param visible
     */
    public void addOrUpdateView(final int childPosition, String value, boolean visible) {
        FragmentDataHolderView fragmentDataHolderView = (FragmentDataHolderView) findViewWithTag(childPosition);
        if (fragmentDataHolderView != null) {
            fragmentDataHolderView.updateView(value);
            if (visible)
                fragmentDataHolderView.setVisibility(VISIBLE);
            else
                fragmentDataHolderView.setVisibility(GONE);
        } else {
            fragmentDataHolderView = new FragmentDataHolderView(getContext(), value);
            fragmentDataHolderView.setTag(childPosition);
            fragmentDataHolderView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) getContext()).onFragmentPlaceholderClicked(childPosition);
                }
            });
            if (visible)
                fragmentDataHolderView.setVisibility(VISIBLE);
            else
                fragmentDataHolderView.setVisibility(GONE);
            addView(fragmentDataHolderView);
        }
    }

    /**
     * Hiding placeholder view when the view associated to the placeholder is loaded and showing all the other placeholder views.
     *
     * @param childPosition
     */
    public void hideFragmentPlaceHolderView(int childPosition) {
        FragmentDataHolderView fragmentDataHolderView = (FragmentDataHolderView) findViewWithTag(childPosition);
        if (fragmentDataHolderView != null) {
            fragmentDataHolderView.setVisibility(View.GONE);

            int childCount = getChildCount();

            for (int i = 0; i < childCount; i++) {
                if (i != childPosition) {
                    getChildAt(i).setVisibility(VISIBLE);
                }

            }
        }
    }

}
