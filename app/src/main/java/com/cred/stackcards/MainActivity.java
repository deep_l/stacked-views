package com.cred.stackcards;

import android.os.Bundle;
import android.widget.Toast;

import com.cred.stackcards.customviews.LinearDataLayout;
import com.cred.stackcards.listener.FragmentPlaceholderClickListener;
import com.cred.stackcards.model.FragmentDataModel;
import com.cred.stackcards.ui.main.PageViewModel;
import com.cred.stackcards.ui.main.PlaceholderFragment;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.cred.stackcards.Constants.ARG_FRAGMENT_HINT;
import static com.cred.stackcards.Constants.ARG_FRAGMENT_ID;
import static com.cred.stackcards.Constants.ARG_PLACEHOLDER_FRAGMENT_TITLE;
import static com.cred.stackcards.Constants.ARG_FRAGMENT_TITLE;

public class MainActivity extends AppCompatActivity implements FragmentPlaceholderClickListener {

    @BindView(R.id.customLinearLayout)
    LinearDataLayout linearDataLayout;
    List<FragmentDataModel> fragmentDataModelList = new ArrayList<>();
    private int currentFragmentId = 0; //maintaining id of the current visible fragment to validate when back button is pressed.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initializeFragmentData();
        loadFragment(0);
        setupObservers();
    }


    private void setupObservers() {
        PageViewModel pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        pageViewModel.getFragmentLiveData().observe(this, new Observer<FragmentDataModel>() {
            @Override
            public void onChanged(FragmentDataModel fragmentDataModel) {
                int fragmentId = fragmentDataModel.getFragmentId();
                FragmentDataModel mFragmentDataModel = fragmentDataModelList.get(fragmentDataModel.getFragmentId());
                String fragmentPlaceholderTitle = fragmentDataModel.getFragmentPlaceholderTitle();
                if (mFragmentDataModel != null) {
                    mFragmentDataModel.setFragmentPlaceholderTitle(fragmentPlaceholderTitle);

                    //validating if this is the last fragment in the initialized fragment data
                    if (fragmentId < fragmentDataModelList.size() - 1) {
                        //loading the next fragment
                        loadFragment(fragmentId + 1);
                        currentFragmentId = fragmentId + 1;
                        linearDataLayout.addOrUpdateView(fragmentId, fragmentPlaceholderTitle, true);
                    } else {
                        //capturing the value for placeholder view but hiding view if this is the last fragment.
                        linearDataLayout.addOrUpdateView(fragmentId, fragmentPlaceholderTitle, false);
                        Toast.makeText(MainActivity.this, "Registration Successful. Your data is safe with us as we're not associated to Cambridge Analytica.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    /**
     * initialising data i.e. supposed to be used within each Fragment.
     */
    private void initializeFragmentData() {
        fragmentDataModelList.add(new FragmentDataModel("Enter your name here", "Welcome to SIGNAL.\nPlease enter your name to proceed."));
        fragmentDataModelList.add(new FragmentDataModel("Enter your phone number here", "Please enter your phone number.\n(Don't worry we won't share it with anyone)"));
        fragmentDataModelList.add(new FragmentDataModel("Enter your city here", "FINAL STEP.\n By clicking on Submit you agree to accept our terms and condition.\n\nP.S. We won't delete you account if you don't agree them."));
    }


    /**
     *
     * Loads the fragment with the required fragments.
     *
     * @param fragmentId
     */
    private void loadFragment(int fragmentId) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        PlaceholderFragment placeholderFragment = PlaceholderFragment.newInstance();
        FragmentDataModel fragmentDataModel = fragmentDataModelList.get(fragmentId);
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_FRAGMENT_ID, fragmentId);
        bundle.putString(ARG_PLACEHOLDER_FRAGMENT_TITLE, fragmentDataModel.getFragmentPlaceholderTitle());
        bundle.putString(ARG_FRAGMENT_TITLE, fragmentDataModel.getFragmentTitle());
        bundle.putString(ARG_FRAGMENT_HINT, fragmentDataModel.getFragmentHint());
        placeholderFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frameLayout, placeholderFragment);
        fragmentTransaction.commit();
        linearDataLayout.hideFragmentPlaceHolderView(fragmentId);
    }


    /**
     * onClicklistener for Placeholder view.
     * @param fragmentId
     */
    @Override
    public void onFragmentPlaceholderClicked(int fragmentId) {
        loadFragment(fragmentId);
    }

    @Override
    public void onBackPressed() {
        if (currentFragmentId > 0) {
            currentFragmentId--;
            loadFragment(currentFragmentId);
        } else {
            super.onBackPressed();
        }
    }
}